using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneManager : MonoBehaviour
{
    public void StartGame ()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(1);
    }

    public void BackToStart () {
        UnityEngine.SceneManagement.SceneManager.LoadScene(0);
    }
}
