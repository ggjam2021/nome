using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomChar : MonoBehaviour
{
    static char[] chars;
    
    private void Awake() {
        if (chars == null) {
            chars = "$%#@!*abcdefghijklmnopqrstuvwxyz1234567890?;:ABCDEFGHIJKLMNOPQRSTUVWXYZ^&".ToCharArray();
        }
        int index = Random.Range(0, chars.Length);
        GetComponent<TMPro.TextMeshProUGUI>().text = chars[index].ToString();
        enabled = false;
    }
}
