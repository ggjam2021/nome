using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldMovementBounds : MonoBehaviour
{
    public static float LEFT, RIGHT, UP, DOWN;

    public float leftLimit, rightLimit, upLimit, downLimit;

    private void Awake() {
        UP = upLimit;
        DOWN = downLimit;
        LEFT = leftLimit;
        RIGHT = rightLimit;
    }

    void OnDrawGizmos() {
        //Draw bounds
        Gizmos.color = new Color(1, 0.25f, 0);
        Gizmos.DrawLine(new Vector2(leftLimit, -10), new Vector2(leftLimit, 10));
        Gizmos.color = new Color(1, 0, 0.25f);
        Gizmos.DrawLine(new Vector2(rightLimit, -10), new Vector2(rightLimit, 10));
        Gizmos.color = new Color(0, 1, 0.35f);
        Gizmos.DrawLine(new Vector2(-100, upLimit), new Vector2(100, upLimit));
        Gizmos.color = new Color(0.35f, 1, 0);
        Gizmos.DrawLine(new Vector2(-100, downLimit), new Vector2(100, downLimit));
    }

    private void OnValidate() {
        UP = upLimit;
        DOWN = downLimit;
        LEFT = leftLimit;
        RIGHT = rightLimit;
    }
}
