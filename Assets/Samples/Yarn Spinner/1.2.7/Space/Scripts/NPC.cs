﻿/*

The MIT License (MIT)

Copyright (c) 2015-2017 Secret Lab Pty. Ltd. and Yarn Spinner contributors.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*/

using System.Collections;
using System.Linq;
using UnityEngine;

namespace Yarn.Unity.Example {
    /// attached to the non-player characters, and stores the name of the Yarn
    /// node that should be run when you talk to them.

    public class NPC : MonoBehaviour {
        public TMPro.TextMeshPro nameDisplay;

        public string characterName = "";

        public string talkToNode = "";

        public bool disableOnDialogueStart = false;

        [Header("Optional")]
        public YarnProgram scriptToLoad;

        string yarnVarName;
        DialogueRunner dialogueRunner;

        void Start () {
            dialogueRunner = FindObjectOfType<Yarn.Unity.DialogueRunner>();

            if (scriptToLoad != null) {
                dialogueRunner.Add(scriptToLoad);
            }

            UpdatedName();
        }

        public void DialogueStarted() {
            if (disableOnDialogueStart) {
                gameObject.SetActive(false);
            }

            dialogueRunner.AddFunction("getNPCObjectName", 0, delegate (Yarn.Value[] parameters)
            {
                return name;
            });
            dialogueRunner.AddFunction("getNPCName", 0, delegate (Yarn.Value[] parameters)
            {
                return characterName;
            });
            dialogueRunner.onDialogueComplete.AddListener(RemoveGetNameFunction);
        }
        void RemoveGetNameFunction() {
            dialogueRunner.RemoveFunction("getNPCObjectName");
            dialogueRunner.RemoveFunction("getNPCName");
            dialogueRunner.onDialogueComplete.RemoveListener(RemoveGetNameFunction);
        }


        [YarnCommand("moveBy")]
        public void MoveBy(string amount, string time) {
            var split = amount.Split(',');
            Vector3 movement = new Vector3(float.Parse(split[0]), float.Parse(split[1]), float.Parse(split[2]));
            if (moveRoutine != null) {
                StopCoroutine(moveRoutine);
            }
            moveRoutine = StartCoroutine(MoveByRoutine(movement, float.Parse(time)));
        }

        Coroutine moveRoutine;
        IEnumerator MoveByRoutine(Vector3 moveAmount, float time) {
            float lerpTime = 0;
            time = Mathf.Clamp(time, 0.01f, 9999f);
            Vector3 startingPos = transform.position;
            while (lerpTime < 1) {
                transform.position = Vector3.Lerp(startingPos, startingPos + moveAmount, lerpTime);
                lerpTime += Time.deltaTime / time;
                yield return null;
            }
            transform.position = startingPos + moveAmount;
        }

        [YarnCommand("linkVarName")]
        public void LinkNameToVar(string varName) {
            yarnVarName = varName;
            UpdatedName();
        }

        [YarnCommand("getLetterCount")]
        public void GetLetterCount(string varName, string letter) {
            if (!string.IsNullOrWhiteSpace(yarnVarName)) {
                var character = char.Parse(letter.ToLowerInvariant());
                var count = characterName.ToLowerInvariant().Count(f => f == character);
                dialogueRunner.variableStorage.SetValue("$" + varName, new Yarn.Value(count));
            }
        }
        [YarnCommand("getNameLength")]
        public void GetNameLength(string varName) {
            if (!string.IsNullOrWhiteSpace(yarnVarName)) {
                dialogueRunner.variableStorage.SetValue("$" + varName, new Yarn.Value(characterName.Length));
            }
        }

        [YarnCommand("addLetter")]
        public void AddLetter(string letter) {
            characterName += letter;
            UpdatedName();
        }
        [YarnCommand("addLetterFromVar")]
        public void AddLetterFromVar(string varName) {
            var letter = dialogueRunner.variableStorage.GetValue(varName).AsString;
            characterName += letter;
            UpdatedName();
        }
        [YarnCommand("addLetterAtIndex")]
        public void AddLetter(string letter, string atIndex) {
            if (!int.TryParse(atIndex, out var index)) {
                Debug.LogError("Index is Not a Number");
                return;
            }
            if (index >= 0 && index < characterName.Length) {
                characterName = characterName.Insert(index, letter);
            }
            else if (index < 0) {
                characterName = characterName.Insert(0, letter);
            }
            else {
                characterName += letter;
            }

            UpdatedName();
        }

        [YarnCommand("removeLastLetter")]
        public void RemoveLetter() {
            RemoveLetter((characterName.Length - 1).ToString());
        }
        [YarnCommand("removeLetterAtIndex")]
        public void RemoveLetter(string atIndex) {
            if (!int.TryParse(atIndex, out var index)) {
                Debug.LogError("Index is Not a Number");
                return;
            }
            if (index >= characterName.Length) {
                index = characterName.Length - 1;
            }
            if (index < 0) {
                return;
            }
            characterName = characterName.Remove(index, 1);

            UpdatedName();
        }
        [YarnCommand("removeLetter")]
        public void RemoveFirstOfLetter(string letter) {
            var lowerName = characterName.ToLower();
            var letterChar = char.Parse(letter.ToLower());
            for (int i = 0; i < lowerName.Length; i++) {
                if (lowerName[i] == letterChar) {
                    characterName = characterName.Remove(i, 1);
                    UpdatedName();
                    return;
                }
            }
        }

        [YarnCommand("switchLetter")]
        public void SwitchLetter(string atIndex, string newLetter) {
            if (!int.TryParse(atIndex, out var index)) {
                Debug.LogError("Index is Not a Number");
                return;
            }
            if (characterName.Length <= 0) {
                AddLetter(newLetter);
                return;
            }
            characterName = characterName.Insert(index, newLetter);
            characterName = characterName.Remove(index + newLetter.Length, 1);

            UpdatedName();
        }

        [YarnCommand("setNewName")]
        public void SetNameTo(string newName) {
            characterName = newName;
            UpdatedName();
        }

        [YarnCommand("shuffleName")]
        public void ShuffleNameLetters() {
            char[] myChar = characterName.ToCharArray();
            for (int i = myChar.Length - 1; i > 0; i--) {
                int rnd = Random.Range(0, i);
                char temp = myChar[i];
                myChar[i] = myChar[rnd];
                myChar[rnd] = temp;
            }

            characterName = new string(myChar);

            UpdatedName();
        }

        [YarnCommand("endGame")]
        public void EndGame() {
            print("Game ended!");
            GetComponent<PlayerCharacter>().EndGame();
        }

        void UpdatedName() {
            if (!string.IsNullOrWhiteSpace(yarnVarName)) {
                dialogueRunner.variableStorage.SetValue("$" + yarnVarName, new Yarn.Value(characterName));
            }
            if (nameDisplay) {
                nameDisplay.text = characterName;
            }
        }
    }
}
